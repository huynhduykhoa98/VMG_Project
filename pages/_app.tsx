import { ChakraProvider } from '@chakra-ui/react'
import LoginLayout from '../components/loginLayout'
import Layout from './../components/layout'
import { useRouter } from 'next/router'
export default function App({ Component, props }) {
    const router = useRouter();
    if(router.pathname =='/login'){
        return App.getLoginLayout(<Component {...props}></Component>)
    }else{
        return App.getLayout(<Component {...props}></Component>)
    }
}
App.getLayout = function getLayout(page) {
    return (
        <ChakraProvider>
            <Layout>
                {page}
            </Layout>
        </ChakraProvider>

    )
}
App.getLoginLayout = function getLayout(page) {
    return (
        <ChakraProvider>
            <LoginLayout>
                {page}
            </LoginLayout>
        </ChakraProvider>

    )
}